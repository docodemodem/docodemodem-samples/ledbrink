# LEDBrink
サーキットデザイン製プログラマブル無線ユニット「どこでもでむ」を使ったLチカです。

## インストール方法

VSCodeで、File-> Add Folder to Workspaceでフォルダを追加します。

自動でdocodemodemライブラリをgitlabからダウンロードしますが、少々時間がかかります。

そのため直ぐにコンパイルしようとするとエラーになります。

.pioフォルダのlibdepsフォルダを見て、Docodemo Common,DocodeModem Libraryフォルダが作成されるとダウンロードが完了したことを示します。

その後、左バーのPlatformioアイコンをクリックし、Project Tasks -> Miscellaneous -> Rebuild IntelliSense Indexをクリックして、ファイルのパスを再構築します。

.vscodeフォルダにc_cpp_properties.jsonとlaunch.jsonが作成された後、コンパイル可能になります。



## どこでもでむminiで使う場合

platformio.iniを以下のように変更してください。

```
[env:mzeroUSB]
platform = atmelsam
board = mzeroUSB
framework = arduino

monitor_speed = 115200
monitor_port = COM3

upload_port = COM3
;upload_protocol = atmel-ice
;debug_tool = atmel-ice

;board_build.f_cpu = 800000L

lib_extra_dirs = 
;    ../libs/common
;    ../libs/samd21

lib_deps =  
    https://github.com/BriscoeTech/Arduino-FreeRTOS-SAMD21.git
    https://gitlab.com/CircuitDesign1189/docodemo_samd21.git
    https://gitlab.com/CircuitDesign1189/docodemo_common.git

```
