/*
 * LED brink sample programs for DocodeModem
 * Copyright (c) 2020 Circuit Desgin,Inc
 * This software is released under the MIT License.
 * http://opensource.org/licenses/mit-license.php
 */

#include <docodemo.h>

DOCODEMO Dm = DOCODEMO();

void setup()
{
  Dm.begin();
}

void loop()
{
  while (1)
  {
    Dm.LedCtrl(RED_LED, ON);
    Dm.LedCtrl(GREEN_LED, ON);
    delay(1000);

    Dm.LedCtrl(RED_LED, OFF);
    Dm.LedCtrl(GREEN_LED, OFF);
    delay(1000);
  }
} 